package com.guille.formula1;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * Created by willi on 07/11/2015.
 */
@Entity
@Table(name = "escuderia")
public class Escuderia implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String Nombre;
    @Column(name = "director")
    private String Director;
    @Column(name = "pais")
    private String Pais;
    @Column(name = "fecha_creacion")
    private Date FechaCreacion;
    @Column(name = "numero_trabajadores")
    private int NumeroTrabajadores;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String pais) {
        Pais = pais;
    }

    public Date getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public int getNumeroTrabajadores() {
        return NumeroTrabajadores;
    }

    public void setNumeroTrabajadores(int numeroTrabajadores) {
        NumeroTrabajadores = numeroTrabajadores;
    }

    @Override
    public String toString() {
        return Nombre +", " + ", Director: "+ Director;

    }
}
