package com.guille.formula1.Util;

import javax.swing.*;
import java.io.*;

/**
 * Created by willi on 07/11/2015.
 */
public class Util {
    public static int mensajeConfirmacion(String mensaje) {
        return JOptionPane.showConfirmDialog(null, mensaje, "Confirmar", JOptionPane.YES_NO_OPTION);
    }

    public static void mensajeError(String mensaje, String titulo) {
        JOptionPane.showMessageDialog(null,
                mensaje, titulo,
                JOptionPane.ERROR_MESSAGE);
    }
}
