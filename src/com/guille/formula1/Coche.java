package com.guille.formula1;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by willi on 07/11/2015.
 */
@Entity
@Table(name = "coche")
public class Coche implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String Nombre;
    @Column(name = "caballos")
    private int Caballos;
    @Column(name = "coste_coche")
    private float CosteDelCoche;
    @Column(name = "peso_coche")
    private float peso_coche;
    @Column(name = "cilindrada")
    private int cilindrada;

    private String Pilotos;

    public String getPilotos() {
        return Pilotos;
    }

    public void setPilotos(String pilotos) {
        Pilotos = pilotos;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getCaballos() {
        return Caballos;
    }

    public void setCaballos(int caballos) {
        Caballos = caballos;
    }

    public float getCosteDelCoche() {
        return CosteDelCoche;
    }

    public void setCosteDelCoche(float costeDelCoche) {
        CosteDelCoche = costeDelCoche;
    }


    @Override
    public String toString() {
        return Nombre +", caballos: "+ Caballos;
    }
}
